$(document).ready(function() {

  $('.s-table--with-filter input').on('input', function () {
    filterTable($(this).parents('table'));
  });

  function filterTable($table) {
    var $filters = $table.find('.s-table__filters th');
    var $rows = $table.find('.s-table__data');
    $rows.each(function (rowIndex) {
      var valid = true;
      $(this).find('td').each(function (colIndex) {
        if ($filters.eq(colIndex).find('input').val()) {
          if ($(this).html().toLowerCase().indexOf(
          $filters.eq(colIndex).find('input').val().toLowerCase()) == -1) {
            valid = valid && false;
          }
        }
      });
      if (valid === true) {
        $(this).css('display', '');
      } else {
        $(this).css('display', 'none');
      }
    });
  }

  $('.s-inner-layout__more-btn').on('click', function() {
    var button = $(this);
    var text = button.text() == 'Показать больше' ? 'Показать меньше' : 'Показать больше';
    button.text(text);
  });

  svg4everybody();

  var productsSlider = new Swiper('.s-products__slider-swiper', {
    slidesPerView: 3,
    spaceBetween: 30,
    breakpoints: {
      539: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      991: {
        slidesPerView: 2,
        spaceBetween: 30
      }
    },
    navigation: {
      nextEl: '.s-products__slider-btn--next',
      prevEl: '.s-products__slider-btn--prev'
    },
  });

  var promoSlider = new Swiper('.s-promo__slider-swiper', {
    navigation: {
      nextEl: '.s-promo__slider-btn--next',
      prevEl: '.s-promo__slider-btn--prev'
    },
  });

  var categorySlider = new Swiper('.s-category-slider', {
    // pagination: {
    //   el: '.swiper-pagination',
    //   type: 'bullets',
    //   dynamicBullets: true,
    //   clickable: true
    // },
  });

  $('.s-category-slider').lightGallery({
    selector: 'a',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false
  });

  var oilSlider = new Swiper('.s-oil__slider-swiper', {
    slidesPerView: 6,
    spaceBetween: 30,
    breakpoints: {
      539: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      991: {
        slidesPerView: 2,
        spaceBetween: 30
      }
    },
    navigation: {
      nextEl: '.s-products__slider-btn--next',
      prevEl: '.s-products__slider-btn--prev'
    },
  });
});